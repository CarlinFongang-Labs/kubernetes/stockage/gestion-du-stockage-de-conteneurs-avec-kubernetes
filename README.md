# Lab : Utilisation des volumes Kubernetes pour monter du stockage externe sur des conteneurs

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs 

1. Créer un pod qui envoie des données à l'hôte à l'aide d'un volume

2. Créer un pod multi-conteneurs qui partage des données entre conteneurs à l'aide d'un volume


# Contexte

Votre entreprise, BeeHive, développe certaines applications dont les besoins de stockage vont au-delà du stockage de courte durée du système de fichiers conteneur. Un composant, un simple script de maintenance, doit pouvoir interagir avec un répertoire du système de fichiers hôte. Un autre doit pouvoir partager des données entre deux conteneurs dans le même Pod.

Votre tâche consiste à créer une architecture simple pour démontrer comment ces tâches peuvent être accomplies avec les volumes Kubernetes. Créez des pods qui répondent aux critères spécifiés.

>![Alt text](img/image.png)


# Application


## Introduction
Les volumes Kubernetes offrent un moyen simple de monter du stockage externe sur des conteneurs. Ce laboratoire testera vos connaissances sur les volumes lorsque nous assurerez le stockage de certains conteneurs selon une spécification fournie. Cela nous permettra de mettre en pratique ce que nous savez sur l'utilisation des volumes Kubernetes.


## Étape 1 : Connexion au serveur du Plan de Contrôle

Connectez-nous au serveur du plan de contrôle à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Créer un Pod qui envoie des données à l'Hôte à l'aide d'un volume

1. Créez un pod qui interagira avec le système de fichiers hôte :

```sh
nano maintenance-pod.yml
```

2. Entrez la première partie du YAML de base pour un simple pod occupé qui génère des données toutes les cinq secondes sur le disque de l'hôte :

```yaml
apiVersion: v1
kind: Pod
metadata:
    name: maintenance-pod
spec:
  containers:
  - name: busybox
    image: busybox
    command: ['sh', '-c', 'while true; do echo Success! >> /output/output.txt; sleep 5; done']
```

3. Sous le YAML de base, commencez à créer des volumes, qui doivent être au niveau des spécifications des conteneurs :

```yaml
  volumes:
  - name: output-vol
    hostPath:
      path: /var/data
```

4. Dans la spécification des conteneurs du YAML de base, ajoutez une ligne pour les montages de volumes :

```yaml
volumeMounts:
- name: output-vol
  mountPath: /output
```

5. Vérifiez que le fichier final `maintenance-pod.yml` ressemble à ceci :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: maintenance-pod
spec:
  containers:
  - name: my-container
    image: busybox
    command: ['sh', '-c', 'while true; do echo Success! >> /output/output.txt; sleep 5; done']
    volumeMounts:
    - name: output-vol
      mountPath: /output
  volumes:
  - name: output-vol
    hostPath:
      path: /var/data
```

6. Enregistrez le fichier et quittez 

7. Terminez la création du pod :

```sh
kubectl create -f maintenance-pod.yml
```

>![Alt text](img/image-1.png)
*Création du pod réussi*

8. Assurez-nous que le Pod est opérationnel :

```sh
kubectl get pods
```

>![Alt text](img/image-2.png)
*Pod en cours d'exécution*

9. Vérifiez que `maintenance-pod` est en cours d'exécution, il devrait donc envoyer des données au système hôte.

```sh
kubectl get pods maintenance-pod -o wide
```

>![Alt text](img/image-3.png)

On peux ainsi identifier sur quel worker se trouve le pod

## Étape 3 : Vérifier les Données sur le Système Hôte

1. Connectez-nous au serveur du nœud de travail à l'aide des informations d'identification fournies :

```sh
ssh cloud_user@PUBLIC_IP_ADDRESS_WORKER_NODE
```

2. Examinez le résultat pour voir si la configuration du pod a réussi :

```sh
cat /var/data/output.txt
```

>![Alt text](img/image-4.png)
*Le pod rempli bien son role*

## Étape 4 : Créer un Pod multi-conteneurs qui partage des données entre conteneurs à l'aide d'un volume

1. Revenez au serveur du plan de contrôle.

2. Créez un autre fichier YAML pour un pod multi-conteneurs de données partagées :

```sh
nano shared-data-pod.yml
```

3. Commencez par la définition de base du Pod et ajoutez plusieurs conteneurs, où le premier conteneur écrira dans le fichier `output.txt` et le deuxième conteneur le lira :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: shared-data-pod
spec:
  containers:
  - name: my-container1
    image: busybox
    command: ['sh', '-c', 'while true; do echo Success! >> /output/output.txt; sleep 5; done']
  - name: my-container2
    image: busybox
    command: ['sh', '-c', 'while true; do cat /input/output.txt; sleep 5; done']
```

4. Configurez les volumes au même niveau que les conteneurs, avec un volume `emptyDir` qui n'existe que pour partager des données entre deux conteneurs :

```yaml
  volumes:
  - name: shared-vol
    emptyDir: {}
```

5. Montez ce volume entre les deux conteneurs en ajoutant les lignes suivantes sous `command` pour le conteneur `busybox1` :

```yaml
    volumeMounts:
    - name: shared-vol
      mountPath: /output
```

6. Pour le conteneur `busybox2`, ajoutez les lignes suivantes sous `command` pour monter le même volume :

```yaml
    volumeMounts:
    - name: shared-vol
      mountPath: /input
```

7. Assurez-nous que le fichier final `shared-data-pod.yml` ressemble à ceci :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: shared-data-pod
spec:
  containers:
  - name: my-container1
    image: busybox
    command: ['sh', '-c', 'while true; do echo Success! >> /output/output.txt; sleep 5; done']
    volumeMounts:
    - name: shared-vol
      mountPath: /output
  - name: my-container2
    image: busybox
    command: ['sh', '-c', 'while true; do cat /input/output.txt; sleep 5; done']
    volumeMounts:
    - name: shared-vol
      mountPath: /input
  volumes:
  - name: shared-vol
    emptyDir: {}
```

8. Enregistrez le fichier et quittez 

9. Terminez la création du pod multi-conteneurs :

```sh
kubectl create -f shared-data-pod.yml
```

>![Alt text](img/image-5.png)
*Création réussie*

10. Assurez-nous que le Pod est opérationnel et vérifiez si les deux conteneurs sont exécutés et prêts :

```sh
kubectl get pods
```

>![Alt text](img/image-6.png)
*Pod en cours d'exécution*

11. Pour nous assurer que le Pod fonctionne, vérifiez les journaux et spécifiez le deuxième conteneur qui lit les données et les imprime sur la console :

```sh
kubectl logs shared-data-pod -c my-container2
```
>![Alt text](img/image-7.png)


- Si nous voyons une série de messages "Success!", nous avons créé avec succès les deux conteneurs, l'un utilisant un volume de chemin d'hôte pour écrire des données sur le disque hôte et l'autre utilisant un volume `emptyDir` pour partager un volume entre deux conteneurs dans le même Pod.